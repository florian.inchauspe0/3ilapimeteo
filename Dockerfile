#FROM node

#RUN apt-get update && apt-get upgrade -y \
    #&& apt-get clean

#RUN mkdir /app

# Create app directory
#WORKDIR /app

#COPY package*.json /app/

#RUN npm install --only=production

# Bundle app source
#COPY src /app/src

#EXPOSE 3000

#CMD [ "node", "index.js" ]

FROM node:14

# Create app directory
WORKDIR /app

COPY src/package*.json ./

RUN npm install
RUN npm install node-fetch@2

# Bundle app source
COPY ./src .

EXPOSE 3000

CMD [ "node", "index.js" ]

