Will be a dump for the cmd commands and path for the API for now
If you see this with the final commit .... I forgor. 

Users : 
    http://IP:3000/users/create // POST -> Use body for param 
    http://IP:3000/users/update // Update -> Use body for param 
    http://IP:3000/users/delete // Delete -> Use body for param 
    http://IP:3000/users/login // Get -> Use body for param 

meteorologie : 
    http://IP:3000/meteorologie/forecast/:numbersOfDay/:latitude/:longitude/:locationName                    // GET 
    http://IP:3000/meteorologie/forecast/:ville/:numbersOfDay                                                // GET
    http://IP:3000/meteorologie/temperatures/:numbersOfDay/:latitude/:longitude/:locationName                // GET
    http://IP:3000/meteorologie/temperatures/:ville/:numbersOfDay                                            // GET
    http://IP:3000/meteorologie/todayWeather/:latitude/:longitude/:locationName                              // GET
    http://IP:3000/meteorologie/todayWeather/:ville                                                          // GET
    http://IP:3000/meteorologie/previousWeatherReports/:numbersOfPastDay/:latitude/:longitude/:locationName  // GET
    http://IP:3000/meteorologie/previousWeatherReports/:numbersOfPastDay/:ville                              // GET

        ville -> String which is the name of the location where you want to see the weather
        latitude -> float which is you latitude coordinates
        longitude -> float which is you longitude coordinates
        numbersOfDay -> int which should only be 7 max (will not show data past the 7 days mark)
        numbersOfPastDay -> int which should only be 31 max (will not show data past the 31 days mark)
        locationName -> name to which the latitude and longitude while be associated with

cmd :
    for docker
        database :
            docker-compose up -d node_db

        node : 
            sudo docker-compose up node_app
            sudo docker-compose build

Usefull links:
    base of the project :
        https://dev.to/francescoxx/build-a-crud-rest-api-in-javascript-using-nodejs-express-postgres-docker-jkb
