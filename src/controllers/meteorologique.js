const fetch = require("node-fetch");
const UserApiRequest = require('../models/userApiRequest');
const Coordinates = require('../models/coordinates');
const { json } = require("sequelize");

// Get weatherForcast: param { latitude , longitude , numbersOfDay } | task : get the weather for the current day and for the following days at the specifics coordinates | 
exports.getWeatherForcast = async (req, res, next) => {
  try {
    const latitude = parseFloat(req.params.latitude);
    const longitude = parseFloat(req.params.longitude);
    const numbersOfDay = parseInt(req.params.numbersOfDay) ;
    const locationName = req.params.locationName;

    if (locationName == null){
      locationName = "Coordinates";
    }

    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      console.log("numbersOfDay :" , numbersOfDay);
      console.log("https://api.open-meteo.com/v1/forecast?latitude="+latitude+"&longitude="+longitude+"&hourly=temperature_2m,precipitation_probability,precipitation,weathercode,windspeed_10m&forecast_days="+numbersOfDay+"&models=meteofrance_seamless&timezone=Europe%2FBerlin");
      let response = await fetch("https://api.open-meteo.com/v1/forecast?latitude="+latitude+"&longitude="+longitude+"&hourly=temperature_2m,precipitation_probability,precipitation,weathercode,windspeed_10m&forecast_days="+numbersOfDay+"&models=meteofrance_seamless&timezone=Europe%2FBerlin", requestOptions);
        if(response.ok){
          let reponseFinal = await response.json().catch(error => console.log('error', error));
          let test = UserApiRequest.create({
            timestamp: reponseFinal.hourly.time[0],
            daysAhead: numbersOfDay,
            coordinates : {
              latitude: latitude,
              longitude: longitude,
              locationName: locationName,
            },
            PreviousDays: 0
          });

          /***************************DEBUG ****************************************************/
          //console.log('timestamp:' + reponseFinal.hourly.time[0] + '\ndaysAhead:' + numberOfDays + '\nlatitude:' + latitude
          //                  + '\nlongitude:' + longitude + '\nlocationName:' + "coordinates" + '\nPreviousDays:' + 0);
          //const affichage = test.idRequest;
          //console.log('affichage:' + affichage);
          //console.log(reponseFinal);
          /*************************** /DEBUG ****************************************************/

          res.status(200).json(reponseFinal);

        }else{
          res.status(500).json({error_details : "failed to get the fetch from api meteo"});
        }   
  }catch (e){
    console.log('error', e); // debug
    res.status(500).json({error_details : "Internal error , please retry"})
  }
}

// Get townWeatherForcast: param { town , numbersOfDay } | task : get the weather for the current day and for the following days at the specifics coordinates | 
exports.getTownWeatherForcast = async (req, res, next) => {
  try {
    const town = req.params.town;
    const numbersOfDay = req.params.numbersOfDay;

    let coordinates = await Coordinates.findOne({ where : { locationName : town }})
    console.log("coordinates :" , coordinates);
        if (!coordinates) {
            return res.status(404).json({ message: 'town not found!' });
        }else{
          var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          let response = await fetch("https://api.open-meteo.com/v1/forecast?latitude="+coordinates.latitude+"&longitude="+coordinates.longitude+"&hourly=temperature_2m,precipitation_probability,precipitation,weathercode,windspeed_10m&forecast_days="+numbersOfDay+"&models=meteofrance_seamless&timezone=Europe%2FBerlin", requestOptions);
          if(response.ok){
            let reponseFinal = await response.json().catch(error => console.log('error', error));
            res.status(200).json({reponseFinal });
          }
        }
  }catch (e){
    console.log('error', e); // debug
    res.status(500).json({error_details : "Internal error , please retry" + numbersOfDay + " 2 " + latitude + " 3 " + longitude })
  }
}

//Get temperature : param { latitude , longitude , numbersOfDay } | task : get the temperature and windspeed forcast for up to the 4th day ahead | 

exports.getTemperature = async (req, res, next) => {
        try {
          const numbersOfDay = parseInt(req.params.numbersOfDay);
          const latitude = parseFloat(req.params.latitude);
          const longitude = parseFloat(req.params.longitude);
          const locationName = req.params.locationName;

          if (locationName == null){
            locationName = "Coordinates";
          }

          var requestOptions = {
              method: 'GET',
              redirect: 'follow'
            };
            console.log("numbersOfDay : " , numbersOfDay);
            console.log("https://api.open-meteo.com/v1/forecast?latitude="+latitude+"&longitude="+longitude+"&hourly=temperature_2m,windspeed_10m&forecast_days="+numbersOfDay+"&models=meteofrance_seamless&timezone=Europe%2FBerlin");
            let response = await fetch("https://api.open-meteo.com/v1/forecast?latitude="+latitude+"&longitude="+longitude+"&hourly=temperature_2m,windspeed_10m&forecast_days="+numbersOfDay+"&models=meteofrance_seamless&timezone=Europe%2FBerlin", requestOptions);
              if(response.ok){
                let reponseFinal = await response.json().catch(error => console.log('error', error));
                let test = UserApiRequest.create({
                  timestamp: reponseFinal.hourly.time[0],
                  daysAhead: numbersOfDay,
                  coordinates : {
                    latitude: latitude,
                    longitude: longitude,
                    locationName: locationName,
                  },
                  PreviousDays: 0
                });
      
                /***************************DEBUG ****************************************************/
                //console.log('timestamp:' + reponseFinal.hourly.time[0] + '\ndaysAhead:' + numberOfDays + '\nlatitude:' + latitude
                //                  + '\nlongitude:' + longitude + '\nlocationName:' + "coordinates" + '\nPreviousDays:' + 0);
                //const affichage = test.idRequest;
                //console.log('affichage:' + affichage);
                //console.log(reponseFinal);
                /*************************** /DEBUG ****************************************************/
      
                res.status(200).json(reponseFinal);
      
              }else{
                res.status(500).json({error_details : "failed to get the fetch from api meteo"});
              }   
        }catch (e){
          console.log('error', e); // debug
          res.status(500).json({error_details : "Internal error , please retry" + numbersOfDay + " 2 " + latitude + " 3 " + longitude })
        }
}

//Get townTemperature : param { town , numbersOfDay } | task : get the temperature and windspeed forcast for up to the 4th day ahead | 

exports.getTownTemperature = (req, res, next) => {
    const town = req.params.town;
    const numbersOfDay = req.params.numbersOfDay;
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m,windspeed_10m&forecast_days="+numbersOfDay+"&models=meteofrance_seamless&timezone=Europe%2FBerlin", requestOptions)
        .then(response => response.json())
        .then(result => {
            res.status(200).json({ result });
        })
        .catch(error => console.log('error', error));
}

// Get weatherToday :  param { latitude , longitude } | task : get the weather forcast for the current day | 
exports.getWeatherToday = async (req, res, next) => {
        try {
          const latitude = parseFloat(req.params.latitude);
          const longitude = parseFloat(req.params.longitude);
          const locationName = req.params.locationName;

          if (locationName == null){
            locationName = "Coordinates";
          }
          var requestOptions = {
              method: 'GET',
              redirect: 'follow'
            };
            
            let response = await  fetch("https://api.open-meteo.com/v1/forecast?latitude="+latitude+"&longitude="+longitude+"&hourly=temperature_2m,precipitation_probability,precipitation,weathercode,windspeed_10m&forecast_days="+0+"&models=meteofrance_seamless&forecast_days=1&timezone=Europe%2FBerlin", requestOptions);
              if(response.ok){
                let reponseFinal = await response.json().catch(error => console.log('error', error));
                let test = UserApiRequest.create({
                  timestamp: reponseFinal.hourly.time[0],
                  daysAhead: 0,
                  coordinates : {
                    latitude: latitude,
                    longitude: longitude,
                    locationName: locationName,
                  },
                  PreviousDays: 0
                });
      
                /***************************DEBUG ****************************************************/
                //console.log('timestamp:' + reponseFinal.hourly.time[0] + '\ndaysAhead:' + numberOfDays + '\nlatitude:' + latitude
                //                  + '\nlongitude:' + longitude + '\nlocationName:' + "coordinates" + '\nPreviousDays:' + 0);
                //const affichage = test.idRequest;
                //console.log('affichage:' + affichage);
                //console.log(reponseFinal);
                /*************************** /DEBUG ****************************************************/
      
                res.status(200).json(reponseFinal);
      
              }else{
                res.status(500).json({error_details : "failed to get the fetch from api meteo"});
              }   
        }catch (e){
          console.log('error', e); // debug
          res.status(500).json({error_details : "Internal error , please retry"})
        }
}
// Get townWeatherToday :  param { town } | task : get the weather forcast for the current day | 
exports.getTownWeatherToday = (req, res, next) => {
    const town = req.params.town;
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m,precipitation_probability,precipitation,weathercode,windspeed_10m&models=meteofrance_seamless&forecast_days=1&timezone=Europe%2FBerlin", requestOptions)
        .then(response => response.json())
        .then(result => {
            res.status(200).json({ result });
        })
        .catch(error => console.log('error', error));
}

//Get lastWeeksWeather :  param { latitude , longitude , numbersOfPastDay} | task : get the weather forcast for the current day and the past weather for the number of day specified |
exports.getLastWeeksWeather= async (req, res, next) => {
      try {
        const latitude = parseFloat(req.params.latitude);
        const longitude = parseFloat(req.params.longitude);
        const numbersOfPastDay = parseInt(req.params.numbersOfPastDay) ;
        const locationName = req.params.locationName;

          if (locationName == null){
            locationName = "Coordinates";
          }

        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          
          let response = await fetch("https://api.open-meteo.com/v1/forecast?latitude="+latitude+"&longitude="+longitude+"&hourly=temperature_2m,precipitation_probability,precipitation,weathercode,windspeed_10m&models=meteofrance_seamless&past_days="+numbersOfPastDay+"&forecast_days=1&timezone=Europe%2FBerlin", requestOptions);
            if(response.ok){
              let reponseFinal = await response.json().catch(error => console.log('error', error));
              let test = UserApiRequest.create({
                timestamp: reponseFinal.hourly.time[0],
                daysAhead: 0,
                coordinates : {
                  latitude: latitude,
                  longitude: longitude,
                  locationName: locationName,
                },
                PreviousDays: numbersOfPastDay
              });
    
              /***************************DEBUG ****************************************************/
              //console.log('timestamp:' + reponseFinal.hourly.time[0] + '\ndaysAhead:' + numberOfDays + '\nlatitude:' + latitude
              //                  + '\nlongitude:' + longitude + '\nlocationName:' + "coordinates" + '\nPreviousDays:' + 0);
              //const affichage = test.idRequest;
              //console.log('affichage:' + affichage);
              //console.log(reponseFinal);
              /*************************** /DEBUG ****************************************************/
    
              res.status(200).json(reponseFinal);
    
            }else{
              res.status(500).json({error_details : "failed to get the fetch from api meteo"});
            }   
      }catch (e){
        console.log('error', e); // debug
        res.status(500).json({error_details : "Internal error , please retry"})
      }
}

//Get lastWeeksTownWeather :  param { town , numbersOfPastDay} | task : get the weather forcast for the current day and the past weather for the number of day specified |
exports.getLastWeeksTownWeather = (req, res, next) => {
  const town = req.params.town;
  const numbersOfPastDay = parseInt(req.params.numbersOfPastDay) ;
  var requestOptions = {
      method: 'GET',
      redirect: 'follow'
    };
    
    fetch("https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m,precipitation_probability,precipitation,weathercode,windspeed_10m&models=meteofrance_seamless&past_days="+numbersOfPastDay+"&forecast_days=1&timezone=Europe%2FBerlin", requestOptions)
      .then(response => response.json())
      .then(result => {
          res.status(200).json({ result });
      })
      .catch(error => console.log('error', error));
}