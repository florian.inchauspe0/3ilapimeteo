const User = require('../models/user');

// CRUD Controllers

//create user
exports.createUser = (req, res, next) => {
  const userName = req.body.userName;
  const userEmail = req.body.userEmail;
  const userPasswd = req.body.userPasswd;
  if (!!userName && !!userEmail && !!userPasswd){
    User.create({
      name: userName,
      email: userEmail,
      password: userPasswd
    })
      .then(result => {
        console.log('Created User');
        res.status(201).json({
          message: 'User created successfully!',
          user: result
        });
      })
      .catch(err => {
        console.log(err);
      }); 
  }else{
    res.status(400).json({ error_details: "The username / email or password are not specified" });
  }
}

//update user
exports.updateUser = (req, res, next) => {
  const userName = req.body.userName;
  const userEmail = req.body.userEmail;
  const userPasswd = req.body.userPasswd;
  const userName_New = req.body.userName;
  const userEmail_New = req.body.userEmail;
  const userPasswd_New = req.body.userPasswd;

  if(userName_New == null){
    userName_New = userName;
  }
  if (userEmail_New == null){
    userEmail_New = userEmail;
  }
  if (userPasswd_New == null){
    userPasswd_New = userPasswd;
  }

    if (!!userName){ // userName not null

      if(!!userPasswd){// userPasswd not null
        User.findOne({ where : { name : userName , userPasswd : userPasswd}})
        .then(user => {
            if (!user) {
                return res.status(404).json({ message: 'User not found!' });
            }
            user.name = userName_New;
            user.email = userEmail_New;
            user.password = userPasswd_New;
            return user.save();
        }).then(result => {
          res.status(200).json({message: 'User updated!', user: result});
        }).catch(err => console.log(err));
      }else{
        res.status(400).json({ error_details: "The password is not specified" });
      }
      
    }else if(!!userEmail){
  
      if(!!userPasswd){// userPasswd not null
      User.findOne({ where : { name : userEmail , userPasswd : userPasswd}})
        .then(user => {
            if (!user) {
                return res.status(404).json({ message: 'User not found!' });
            }
            user.name = userName_New;
            user.email = userEmail_New;
            user.password = userPasswd_New;
            return user.save();
        }).then(result => {
          res.status(200).json({message: 'User updated!', user: result});
        }).catch(err => console.log(err));
      }else{
        res.status(400).json({ error_details: "The password is not specified" });
      }
  
    }else{
      res.status(400).json({ error_details: "The username / email or password are not specified" });
    }
}

//delete user
exports.deleteUser = (req, res, next) => {
  const userId = req.params.userId;
  User.findByPk(userId)
    .then(user => {
      if (!user) {
        return res.status(404).json({ message: 'User not found!' });
      }
      return User.destroy({
        where: {
          id: userId
        }
      });
    })
    .then(result => {
      res.status(200).json({ message: 'User deleted!' });
    })
    .catch(err => console.log(err));
}

exports.getVerifUser = (req, res, next) => {
  const userName = req.body.userName;
  const userEmail = req.body.userEmail;
  const userPasswd = req.body.userPasswd;

  try {
    if (!!userName){ // userName not null

      if(!!userPasswd){// userPasswd not null
        User.findOne({ where : { name : userName , userPasswd : userPasswd}})
        .then(user => {
            if (!user) {
                return res.status(404).json({ message: 'User not found!' });
            }
            res.status(200).json({ user: user });
        })
        .catch(err => console.log(err));
      }else{
        res.status(400).json({ error_details: "The password is not specified" });
      }
      
    }else if(!!userEmail){
  
      if(!!userPasswd){// userPasswd not null
      User.findOne({ where : { name : userEmail , userPasswd : userPasswd}})
        .then(user => {
            if (!user) {
                return res.status(404).json({ message: 'User not found!' });
            }
            res.status(200).json({ user: user });
        })
        .catch(err => console.log(err));
      }else{
        res.status(400).json({ error_details: "The password is not specified" });
      }
  
    }else{
      res.status(400).json({ error_details: "The username / email or password are not specified" });
    } 
  }catch(e){
    console.log("error :" + e)
  }
}
