const Sequelize = require('sequelize');
const db = require('../util/database');
const Coordinates = require('./coordinates');



const UserApiRequest = db.define('userApiRequester',{
    idRequest: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    idCoordinates : {
        type: Sequelize.INTEGER,
        references:{
            model: Coordinates,
            key: 'idCoordinates'
        }
    },
    timestamp: {
        type : Sequelize.STRING,
        allowNull: false
    },
    daysAhead : Sequelize.INTEGER,
    PreviousDays: Sequelize.INTEGER
});

UserApiRequest.hasOne(Coordinates);
module.exports = UserApiRequest;
