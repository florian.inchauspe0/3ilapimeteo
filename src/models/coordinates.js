const Sequelize = require('sequelize');
const db = require('../util/database');

const Coordinates = db.define('coordinates',{
    idCoordinates: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    latitude: {
        type : Sequelize.STRING,
        allowNull: false
        },
    longitude: {
        type : Sequelize.STRING,
        allowNull: false
        },
    locationName: Sequelize.STRING
});

module.exports = Coordinates;