const controller = require('../controllers/users');
const router = require('express').Router();

// CRUD Routes /users
router.post('/create', controller.createUser); // /users/create
router.put('/update', controller.updateUser); // /users/update
router.delete('/delete', controller.deleteUser); // /users/delete
router.get('/login', controller.getVerifUser); // /users/login

module.exports = router;
