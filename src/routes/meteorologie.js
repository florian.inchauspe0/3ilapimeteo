const controller = require('../controllers/meteorologique');
const router = require('express').Router();

// CRUD Routes /meteorologie/
router.get('/forecast/:numbersOfDay/:latitude/:longitude/:locationName', controller.getWeatherForcast); 
router.get('/forecast/:town/:numbersOfDay', controller.getTownWeatherForcast); 
router.get('/temperatures/:numbersOfDay/:latitude/:longitude/:locationName', controller.getTemperature); 
router.get('/temperatures/:town/:numbersOfDay', controller.getTownTemperature); 
router.get('/todayWeather/:latitude/:longitude/:locationName', controller.getWeatherToday); 
router.get('/todayWeather/:town', controller.getTownWeatherToday); 
router.get('/previousWeatherReports/:numbersOfPastDay/:latitude/:longitude/:locationName', controller.getLastWeeksWeather); 
router.get('/previousWeatherReports/:numbersOfPastDay/:town', controller.getLastWeeksTownWeather); 
module.exports = router;